package com.autopark.services.impl;

import com.autopark.AbstractTest;
import com.autopark.domain.Car;
import com.autopark.domain.CarImage;
import com.autopark.domain.CarType;
import com.autopark.domain.enums.TypeName;
import com.autopark.domain.form.CarForm;
import com.autopark.dto.CarDto;
import com.autopark.mapper.CarMapper;
import com.autopark.services.ManagerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
public class ManagerServiceImplTest extends AbstractTest {
    @Autowired
    CarServiceImpl carService;
    @Autowired
    CarMapper carMapper;
    @Autowired
    ManagerService managerService;
    @BeforeEach
    void init(){
        List<CarType> carTypes = carTypeRepository.findAll();
        if (carTypes.isEmpty()) {
            for (TypeName t: TypeName.values()) {
                carTypes.add(CarType.builder().type(t).build());
            }
            carTypeRepository.saveAll(carTypes);
        }
        CarType sport = carTypeRepository.findByType(TypeName.SPORTCAR);
        CarType cross = carTypeRepository.findByType(TypeName.CROSSOVER);
        CarType hatch = carTypeRepository.findByType(TypeName.HATCHBACK);
        CarType sedan = carTypeRepository.findByType(TypeName.SEDAN);

        Car[] cars = {
                Car
                        .builder()
                        .color("red")
                        .price(1001)
                        .weight(500)
                        .type(sport)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(1002)
                        .weight(1000)
                        .type(cross)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(1003)
                        .weight(1000)
                        .type(hatch)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(400)
                        .weight(1000)
                        .type(sedan)
                        .build()
        };
        carRepository.saveAll(Arrays.asList(cars.clone()));
    }

    @Test
    void updateCar(){
        long id_first = carRepository.findAll().get(0).getId();
        CarForm build = CarForm.builder()
                .color("blue")
                .price(100)
                .type("SPORTCAR")
                .weight(101)
                .build();
        CarDto carDto = managerService.updateCar(id_first, build);
        //Optional<Car> byId = carRepository.findById(Long.valueOf(1));
        assertAll(
                ()->assertEquals(carDto.getPrice(), 100),
                ()->assertEquals(carDto.getColor(), "blue"),
                ()->assertEquals(carDto.getWeight(), 101),
                ()->assertEquals(carDto.getType().getType(),TypeName.SPORTCAR)
                );
    }
    @Test
    void updateCarOneParameter(){
        long id_first = carRepository.findAll().get(0).getId();
        CarForm build = CarForm.builder().weight(101).build();
        managerService.updateCar(id_first, build);
        Optional<Car> byId = carRepository.findById(id_first);
        assertAll(
                ()->assertEquals(byId.get().getPrice(), 1001),
                ()->assertEquals(byId.get().getColor(), "red"),
                ()->assertEquals(byId.get().getWeight(), 101),
                ()->assertEquals(byId.get().getType().getType(),TypeName.SPORTCAR)
        );
    }
    @Test
    void updateCarOneParameterAgain(){
        long id_first = carRepository.findAll().get(0).getId();
        CarForm build = CarForm.builder().type("HATCHBACK").build();
        Optional<Car> byId = carRepository.findById(id_first);
        CarDto carDto = managerService.updateCar(byId.get().getId(), build);
        CarDto carDto1 = carMapper.toDto(carRepository.findById(id_first).get());
        assertAll(
                ()->assertEquals(carDto.getPrice(), 1001),
                ()->assertEquals(carDto.getColor(), "red"),
                ()->assertEquals(carDto.getWeight(), 500),
                ()->assertEquals(carDto.getType().getType(),TypeName.HATCHBACK),
                ()->assertEquals(carDto1.getColor(), carDto.getColor())
        );
    }

    @Test
    void addImage() {
        long id_first = carRepository.findAll().get(0).getId();
        Car c = carRepository.findById(id_first).get();
        List<CarImage> images = c.getImages();
        if(images == null){
            images = new ArrayList<>();
        }
        images.add(CarImage.builder().imageUrl("1.jpg").car(c).build());
        c.setImages(images);
        Car save = carRepository.save(c);

        List<CarImage> images2 = save.getImages();
        if(images2 == null){
            images2 = new ArrayList<>();
        }
        images2.add(CarImage.builder().imageUrl("2.jpg").car(c).build());
        save.setImages(images2);
        Car save1 = carRepository.save(c);
        assertAll(
                ()->assertEquals(save1.getImages().size(), 2)
        );
    }

    @Test
    void addImageMethod(){
        //initial
        long id_first = carRepository.findAll().get(0).getId();
        Optional<Car> car0 = carRepository.findById(id_first);
        List<CarImage> images = car0.get().getImages();

        managerService.addImage(id_first, "url1");
        managerService.addImage(id_first, "url2");

        Optional<Car> car1 = carRepository.findById(id_first);
        List<CarImage> images1 = car1.get().getImages();

        assertAll(
                ()->assertEquals(images, null),
                ()->assertEquals(images1.size(),2)
                );
    }

    @Test
    void addImages() {

    }


}
