package com.autopark.services.impl;


import com.autopark.AbstractTest;
import com.autopark.domain.Role;
import com.autopark.domain.enums.Roles;
import com.autopark.domain.form.UserForm;
import com.autopark.dto.TokenDto;
import com.autopark.exceptions.NoSuchRoleException;
import com.autopark.exceptions.UserNotFoundException;
import com.autopark.exceptions.WrongPasswordException;
import com.autopark.services.SignUpService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

//@Transactional
class LoginServiceImplTest extends AbstractTest {
    @Autowired
    UserServiceImpl userService;
    @Autowired
    SignUpService signUpService;
    @Autowired
    LoginServiceImpl loginService;
    @BeforeEach
    void init(){
        List<Role> roles = roleRepository.findAll();
        if (roles.isEmpty()) {
            for (Roles r : Roles.values()) {
                roles.add(Role.builder().name(r).build());
            }
            roleRepository.saveAll(roles);
        }
        Role role_user = roleRepository.findByName(Roles.ROLE_USER);
        UserForm userForm = UserForm.builder().username("user").password("pass").build();
        signUpService.signUp(userForm, Roles.ROLE_USER);
        userForm = UserForm.builder().username("user_admin").password("pass").build();
        signUpService.signUp(userForm, Roles.ROLE_ADMIN);
    }

    @Test
    void userToken(){
        TokenDto login = loginService.login(UserForm.builder().username("user").password("pass").build());
        System.out.println(login.getValue());

        assertAll(
                //()->assertEquals(userRepository.findByUsername("user").getTokens().size(), 1),
                ()->assertEquals(login.getUser().getUsername(),"user"),
                ()->assertEquals(login.getValue().split(":")[0], "user"),
                ()->assertEquals(login.getValue().split(":")[2], "ROLE_USER")
        );
    }

    @Test
    void adminToken(){
        TokenDto login = loginService.login(UserForm.builder().username("user_admin").password("pass").build());
        System.out.println(login.getValue());

        assertAll(
                ()->assertEquals(login.getUser().getUsername(),"user_admin"),
                ()->assertEquals(login.getValue().split(":")[0], "user_admin"),
                ()->assertEquals(login.getValue().split(":").length, 3),
                ()->assertEquals(login.getValue().split(":")[2], "ROLE_ADMIN")
                );
    }
    @Test
    void wrongPassException(){
        try {
            loginService.login(UserForm.builder().username("user").password("123").build());
        }catch (WrongPasswordException e){
            assertEquals(e.getMessage(), "wrong password");
        }catch (UserNotFoundException e1){
            assertEquals(e1.getMessage(), "no user named u1");
        }
    }

    @Test
    void userNotFound(){
        try {
            loginService.login(UserForm.builder().username("u1").password("123").build());
        }catch (WrongPasswordException e){
            assertEquals(e.getMessage(), "wrong password");
        }catch (UserNotFoundException e1){
            assertEquals(e1.getMessage(), "no user named u1");
        }
    }

    @Test
    void signUpServiceTest(){
        try{
            userService.giveRole("user1", "ROLE_ADMIN");
        }catch (NoSuchRoleException e){
            assertEquals(e.getMessage(), "no role named ROLE_HAHA");
        }catch (UserNotFoundException e1){
            assertEquals(e1.getMessage(), "no user named user1");
        }
    }
}