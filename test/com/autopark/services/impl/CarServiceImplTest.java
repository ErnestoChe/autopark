package com.autopark.services.impl;

import com.autopark.AbstractTest;
import com.autopark.domain.Car;
import com.autopark.domain.CarType;
import com.autopark.domain.enums.TypeName;
import com.autopark.domain.form.CarForm;
import com.autopark.dto.CarDto;
import com.autopark.exceptions.NoSuchCarException;
import com.autopark.exceptions.NoSuchTypeException;
import com.autopark.mapper.CarMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
class CarServiceImplTest extends AbstractTest {

    @Autowired
    CarServiceImpl carService;
    @Autowired
    CarMapper carMapper;
    @BeforeEach
    void init(){
        List<CarType> carTypes = carTypeRepository.findAll();
        if (carTypes.isEmpty()) {
            for (TypeName t: TypeName.values()) {
                carTypes.add(CarType.builder().type(t).build());
            }
            carTypeRepository.saveAll(carTypes);
        }
        CarType sport = carTypeRepository.findByType(TypeName.SPORTCAR);
        CarType cross = carTypeRepository.findByType(TypeName.CROSSOVER);
        CarType hatch = carTypeRepository.findByType(TypeName.HATCHBACK);
        CarType sedan = carTypeRepository.findByType(TypeName.SEDAN);

        Car[] cars = {
                Car
                        .builder()
                        .color("red")
                        .price(1001)
                        .weight(500)
                        .type(sport)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(1002)
                        .weight(1000)
                        .type(cross)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(1003)
                        .weight(1000)
                        .type(hatch)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(400)
                        .weight(1000)
                        .type(sedan)
                        .build()
        };
        carRepository.saveAll(Arrays.asList(cars.clone()));
    }

    @Test
    void addCar(){
        CarForm carForm = CarForm.builder().color("red").price(101).weight(101).type("SEDAN").build();
        CarDto carDto = carService.saveCar(carForm);
        assertEquals(carDto.getType().getType(), TypeName.SEDAN);
    }

    @Test
    void findAllCars() {
        List<CarDto> allCars = carService.findAllCars();
        int size = allCars.size();

        assertEquals(size,4);
    }

    @Test
    void findAllException(){
        String error = "";
        carRepository.deleteAll();
        try{
            List<CarDto> allCars = carService.findAllCars();
        }catch (NoSuchCarException e){
            error = e.getMessage();
        }
        assertEquals(error.toString(), "no car with all car id");
    }

    @Test
    void findAllCarsPrice(){
        List<CarDto> priceCars = carService.findAllCarsPriceGreaterThan(500);
        assertEquals(priceCars.size(), 3);
    }
    @Test
    void findByColor(){
        List<CarDto> priceCars = carService.findByColorEquals("red");

        assertEquals(priceCars.size(), 1);
    }
    @Test
    void findByWeight(){
        List<CarDto> priceCars = carService.findByWeightBetween(0, 1500);
        assertEquals(priceCars.size(), 4);
    }

    @Test
    void findByTypeSedan(){
        List<CarDto> typeCars = carService.findByType("sedan");
        assertEquals(typeCars.get(0).getType().getType().name(), "SEDAN");
    }
    @Test
    void findByTypeHatch(){
        List<CarDto> typeCars = carService.findByType("hatchback");
        assertEquals(typeCars.get(0).getType().getType().name(), "HATCHBACK");
    }
    @Test
    void findByTypeCross(){
        List<CarDto> typeCars = carService.findByType("crossover");
        assertEquals(typeCars.get(0).getType().getType().name(), "CROSSOVER");
    }
    @Test
    void findByTypeSport(){
        List<CarDto> typeCars = carService.findByType("sportcar");
        assertEquals(typeCars.get(0).getType().getType().name(), "SPORTCAR");
    }

    @Test
    void findByNonExistType(){
        String error = "";
        try{
            List<CarDto> typeCars = carService.findByType("chel");
        }catch (NoSuchCarException e){
            error = e.getMessage();
        }catch (NoSuchTypeException e1){
            error = e1.getMessage();
        }
        assertEquals(error, "no type named chel");
    }
    @Test
    void addCarTypeException(){
        String error = "";
        try{
            CarForm carForm = CarForm.builder().color("red").price(101).weight(101).type("NOTYPE").build();
            CarDto carDto = carService.saveCar(carForm);
        }catch (NoSuchTypeException e){
            assertEquals(e.getMessage(), "no type named NOTYPE");
        }
    }
}


//@ExtendWith(r)
//class CarServiceImplTest {
//
//    @InjectMocks
//    CarServiceImpl carService;
//
//    @Mock
//    CarRepository carRepository;
//    @Mock
//    CarMapper carMapper;
//
//    @Test
//    void findAllCars() {
//        // when
//        when(carMapper.toDto(anyObject())).thenReturn(new CarDto());
//        when(carRepository.findAll()).thenReturn(Collections.singletonList(new Car()));
//
//        // then
//        List<CarDto> allCars = carService.findAllCars();
//
//        //throw
//        assertEquals(allCars.size(),1);
//    }
//}
