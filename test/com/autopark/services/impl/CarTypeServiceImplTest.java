package com.autopark.services.impl;


import com.autopark.AbstractTest;
import com.autopark.DataSourceConfig;
import com.autopark.domain.CarType;
import com.autopark.domain.enums.TypeName;
import com.autopark.dto.CarTypeDto;
import com.autopark.services.CarTypeService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CarTypeServiceImplTest extends AbstractTest {

    @Autowired
    private CarTypeService carTypeService;
    @BeforeAll
    void init() throws IOException {
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.getEmbeddedDataSource();
        List<CarType> carTypes = carTypeRepository.findAll();
        if (carTypes.isEmpty()) {
            for (TypeName t: TypeName.values()) {
                carTypes.add(CarType.builder().type(t).build());
            }
            List<CarType> carTypes1 = carTypeRepository.saveAll(carTypes);
        }
    }

    @Test
    void findById(){
        long send = 3;
        CarTypeDto carTypedto = carTypeService.findById(send);
        assertEquals(carTypedto.getType(),"HATCHBACK");
    }
    @Test
    void findAll(){


        List<CarTypeDto> list = carTypeService.findAll();
        for (CarTypeDto t:list) {
            System.out.println(t.getType());
        }
        assertEquals(list.size(), 4);
    }

    @Test
    void findByText(){
        CarTypeDto type = carTypeService.findByType("SEDAN");
        assertEquals(type.getType(), "SEDAN");
    }
    @Test
    void addCarWithGetType(){
        CarType type = carTypeRepository.findByType(TypeName.SEDAN);
        assertEquals(type.getId(), 2);
    }

}

