package com.autopark.services.impl;

import com.autopark.AbstractTest;
import com.autopark.domain.Role;
import com.autopark.domain.User;
import com.autopark.domain.enums.Roles;
import com.autopark.domain.form.UserForm;
import com.autopark.dto.UserDto;
import com.autopark.exceptions.NoSuchRoleException;
import com.autopark.exceptions.UserAlreadyExistException;
import com.autopark.exceptions.UserNotFoundException;
import com.autopark.services.SignUpService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
public class UserServiceImplTest extends AbstractTest {
    @Autowired
    UserServiceImpl userService;
    @Autowired
    SignUpService signUpService;
    @BeforeEach
    void init(){
        List<Role> roles = roleRepository.findAll();
        if (roles.isEmpty()) {
            for (Roles r : Roles.values()) {
                roles.add(Role.builder().name(r).build());
            }
            roleRepository.saveAll(roles);
        }
        UserForm userForm = UserForm.builder().username("user").password("pass").build();
        signUpService.signUp(userForm, Roles.ROLE_USER);
        userForm = UserForm.builder().username("user_admin").password("pass").build();
        signUpService.adminSignUp(userForm);
    }

    @Test
    void signUpUser(){
        UserDto userDto = userService.findByUsername("user");
        assertEquals(userDto.getPassword(), "pass");
    }
    @Test
    void findUserByUsername(){
        UserDto userDto = userService.findByUsername("user");
        System.out.println();
        assertEquals(userDto.getRoles().get(0).getName().name(), Roles.ROLE_USER.name());
    }
    @Test
    void findUsersByRole(){
        List<UserDto> list = userService.findByRole(Roles.ROLE_ADMIN.name());
        System.out.println();
        assertAll(
                ()->assertEquals(list.size(), 1),
                ()->assertEquals(list.get(0).getRoles().size(), 1),
                ()->assertEquals(list.get(0).getRoles().get(0).getName().name(), "ROLE_ADMIN")
                );
    }
    @Test
    void findUsersByNoneExistRole(){
        String err = "";
        try{
            List<UserDto> list = userService.findByRole("sike");
        }catch (NoSuchRoleException e){
            err = e.getMessage();
        }
        assertEquals(err, "no role named sike");
    }

    @Test
    void addMultipleRoles(){
        List<Role> list = new ArrayList<>();
        list.add(roleRepository.findByName(Roles.ROLE_USER));
        list.add(roleRepository.findByName(Roles.ROLE_ADMIN));
        User saved = userRepository.save(
                User.builder()
                        .username("u")
                        .password("p")
                        .roles(list)
                        .build()
        );
        assertAll(
                ()->assertEquals(saved.getRoles().size(), 2)
        );
    }

    @Test
    void findSameUserWithDefRoles(){
        List<Role> roles = new ArrayList<>();
        roles.add(roleRepository.findByName(Roles.ROLE_USER));
        roles.add(roleRepository.findByName(Roles.ROLE_ADMIN));
        User saved = userRepository.save(
                User.builder()
                        .username("u")
                        .password("p")
                        .roles(roles)
                        .build()
        );
        UserDto t_admin = userService.findByRole(Roles.ROLE_ADMIN.name()).get(1);
        UserDto t_user = userService.findByRole(Roles.ROLE_USER.name()).get(1);
        assertAll(
                ()->assertEquals(t_admin.getRoles().size(), 2),
                ()->assertEquals(t_user.getRoles().size(), 2),
                ()->assertEquals(t_admin.getUsername(), t_user.getUsername())
                );
    }
    @Test
    void addRoleToExistingUser(){
        User u = userRepository.findByUsername("user");
        List<Role> roles = u.getRoles();
        Role admin_role = roleRepository.findByName(Roles.ROLE_ADMIN);

        List<Role> roleList = new ArrayList<>();
        roleList.addAll(roles);
        roleList.add(admin_role);
        u.setRoles(roleList);

        User user_new = userRepository.save(u);
        User user = userRepository.findByUsername("user");
        assertAll(
                ()->assertEquals(user_new.getRoles().size(), 2),
                ()->assertEquals(user.getUsername(), "user"),
                ()->assertEquals(user.getRoles().size(), 2)
        );

    }

    @Test
    void addRoleToExistingUser2(){
        UserDto old = userService.findByUsername("user");
        userService.giveRole("user", "ROLE_ADMIN");
        UserDto userDto = userService.findByUsername("user");
        assertAll(
                ()->assertEquals(old.getUsername(), "user"),
                ()->assertEquals(old.getRoles().size(), 1),
                ()->assertEquals(userDto.getUsername(), "user"),
                ()->assertEquals(userDto.getRoles().size(), 2)
        );

    }

    @Test
    void exceptionHandler(){
        String error = "";
        try{
            signUpService.signUp(UserForm.builder().username("user").password("123").build(), Roles.ROLE_USER);
        }catch (UserAlreadyExistException e){
            error = e.getMessage();
            System.out.println(e.getMessage());
        }
        assertEquals(error, "username user already taken");
    }
    @Test
    void exceptionHandlerAdmin(){
        String error = "";
        try{
            signUpService.adminSignUp(UserForm.builder().username("user_admin").password("123").build());
        }catch (UserAlreadyExistException e){
            error = e.getMessage();
            System.out.println(e.getMessage());
        }
        assertEquals(error, "username user_admin already taken");
    }

    @Test
    void findByUsernameException(){
        String error = "";
        try{
            userService.findByUsername("user1");
        }catch (UserNotFoundException e){
            error = e.getMessage();
            System.out.println(e.getMessage());
        }
        assertEquals(error, "no user named user1");
    }

    @Test
    void giveRoleUsernameException(){
        String error = "";
        try{
            userService.giveRole("user1", "ROLE_USER");
        }catch (UserNotFoundException e){
            error = e.getMessage();
            System.out.println(e.getMessage());
        }
        assertEquals(error, "no user named user1");
    }
}

