package com.autopark;

import com.autopark.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AbstractTest {

    @LocalServerPort
    protected int port;

    protected TestRestTemplate testRestTemplate;

    @Autowired
    protected CarRepository carRepository;
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected CarTypeRepository carTypeRepository;
    @Autowired
    protected RoleRepository roleRepository;
    @Autowired
    protected CarImageRepository carImageRepository;

    @AfterEach
    void flush(){
        carImageRepository.deleteAll();
        carImageRepository.flush();
        carRepository.deleteAll();
        carRepository.flush();
        userRepository.deleteAll();
        userRepository.flush();
    }

}
