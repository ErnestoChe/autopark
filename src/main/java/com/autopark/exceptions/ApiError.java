package com.autopark.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Getter
public class ApiError {

    @JsonProperty("message")
    private final String message;

    @JsonProperty("time")
    private final LocalDateTime localDateTime;

    public ApiError(String message) {
        this(message,LocalDateTime.now());
    }

    public ApiError(String message, LocalDateTime localDateTime) {
        this.message = message;
        this.localDateTime = localDateTime;
    }
}
