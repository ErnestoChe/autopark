package com.autopark.exceptions;

public class UserAlreadyExistException extends RuntimeException{

    public static final String cliche = "username %s already taken";

    public UserAlreadyExistException(String message){
        super(String.format(cliche, message));
    }
}
