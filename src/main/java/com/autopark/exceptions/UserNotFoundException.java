package com.autopark.exceptions;

public class UserNotFoundException extends RuntimeException{

    public static final String cliche = "no user named %s";

    public UserNotFoundException(String message){
        super(String.format(cliche, message));
    }
}
