package com.autopark.exceptions;

public class NoSuchCarException extends RuntimeException{
    public static final String cliche = "no car with %s id";

    public NoSuchCarException(String message){
        super(String.format(cliche, message));
    }
}
