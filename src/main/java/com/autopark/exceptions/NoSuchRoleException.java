package com.autopark.exceptions;

public class NoSuchRoleException extends RuntimeException{

    public static final String cliche = "no role named %s";

    public NoSuchRoleException(String message){
        super(String.format(cliche, message));
    }
}
