package com.autopark.exceptions;

public class NoSuchTypeException extends RuntimeException{

    public static final String cliche = "no type named %s";

    public NoSuchTypeException(String message){
        super(String.format(cliche, message));
    }
}
