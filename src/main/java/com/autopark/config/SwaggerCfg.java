package com.autopark.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class SwaggerCfg {

    @Bean
    public Docket swaggerConfig() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/car/**"))
                .apis(RequestHandlerSelectors.basePackage("com.autopark"))
                .build()
                .apiInfo(info());
    }

    private ApiInfo info() {
        return new ApiInfo(
                //title
                "addres book",
                "study project",
                "v0.01",
                "terms url",
                new Contact("Ernest", "yarik.com", "erph0511@gmail.com"),
                "licence",
                "lic url",
                Collections.emptyList()
        );
    }
}
