package com.autopark.config;

import com.autopark.mapper.*;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {

    @Bean
    public CarMapper getCarMapper() {
        return Mappers.getMapper(CarMapper.class);
    }

    @Bean
    public CarTypeMapper getCarTypeMapper() {
        return Mappers.getMapper(CarTypeMapper.class);
    }

    @Bean
    public UserMapper getUserMapper() {
        return Mappers.getMapper(UserMapper.class);
    }

    @Bean
    public RoleMapper getRoleMapper() {
        return Mappers.getMapper(RoleMapper.class);
    }

    @Bean
    public TokenMapper getTokenMapper() {
        return Mappers.getMapper(TokenMapper.class);
    }

    @Bean
    public CarImageMapper getCarImageMapper() {
        return Mappers.getMapper(CarImageMapper.class);
    }
}
