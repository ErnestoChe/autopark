package com.autopark.util;

public class URIS {
    //cars

    public static final String CAR_URI = "/car";

    public static final String ALL_CARS_URI = "/all";

    public static final String ADD_CAR = "/add";

    public static final String BY = "/by";

    public static final String PRICE_CAR_URI = "/price";

    public static final String COLOR_CAR_URI = "/color";

    public static final String TYPE_CAR_URI = "/type";

    public static final String WEIGHT_CAR_URI = "/weight";

    public static final String ID_CAR_URI = "/id";

    //users

    public static final String USER_URI = "/user";

    public static final String USER_NAME = "/name";

    public static final String USER_SIGNUP = "/signup";

    public static final String ADMIN_SIGNUP = "/adminsignup";

    public static final String ADD_ROLE = "/role";

    //security

    public static final String AUTH = "/auth";

    //manager paths

    public static final String MANAGER_URI = "/manager";

    public static final String UPDATE_CAR = "/update-info";

    public static final String SELL_CAR = "/sell";

    public static final String ADD_IMAGE = "/image";
}
