package com.autopark.util;

import com.autopark.domain.CarType;
import com.autopark.domain.Role;
import com.autopark.domain.enums.Roles;
import com.autopark.domain.enums.TypeName;
import com.autopark.repository.CarTypeRepository;
import com.autopark.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DataUtil {

    private final CarTypeRepository carTypeRepository;

    private final RoleRepository roleRepository;

    public void initIfDoesNotExist() {
        List<CarType> carTypes = carTypeRepository.findAll();
        if (carTypes.isEmpty()) {
            for (TypeName t : TypeName.values()) {
                carTypes.add(CarType.builder().type(t).build());
            }
            carTypeRepository.saveAll(carTypes);
        }
        List<Role> roles = roleRepository.findAll();
        if (roles.isEmpty()) {
            for (Roles r : Roles.values()) {
                roles.add(Role.builder().name(r).build());
            }
            roleRepository.saveAll(roles);
        }
    }
}

