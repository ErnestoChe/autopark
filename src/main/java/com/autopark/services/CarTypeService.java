package com.autopark.services;

import com.autopark.dto.CarTypeDto;

import java.util.List;

public interface CarTypeService {

    CarTypeDto findById(long id);

    List<CarTypeDto> findAll();

    CarTypeDto findByType(String type);
}
