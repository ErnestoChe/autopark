package com.autopark.services;

import com.autopark.domain.CarImage;
import com.autopark.domain.form.CarForm;
import com.autopark.dto.CarDto;
import com.autopark.dto.CarImageDto;

import java.util.List;

public interface ManagerService {

    CarDto updateCar(long id, CarForm carForm);

    void sellCar(long id);

    CarDto addImage(long id, String url);

    List<CarImageDto> getImages(long id);
}
