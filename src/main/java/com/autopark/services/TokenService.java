package com.autopark.services;

import com.autopark.dto.TokenDto;

public interface TokenService {

    TokenDto findByUsername(String username);

}
