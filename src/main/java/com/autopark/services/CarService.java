package com.autopark.services;

import com.autopark.domain.form.CarForm;
import com.autopark.dto.CarDto;

import java.util.List;

public interface CarService {

    List<CarDto> findAllCars();

    List<CarDto> findAllCarsPriceGreaterThan(int price);

    List<CarDto> findByColorEquals(String color);

    List<CarDto> findByType(String type);

    List<CarDto> findByWeightBetween(int from, int to);

    CarDto saveCar(CarForm carForm);

    CarDto findById(long id);
}
