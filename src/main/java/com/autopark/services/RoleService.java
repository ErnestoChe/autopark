package com.autopark.services;

import com.autopark.dto.RoleDto;

public interface RoleService {

    RoleDto findByName(String name);

}
