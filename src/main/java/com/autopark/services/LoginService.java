package com.autopark.services;

import com.autopark.domain.form.UserForm;
import com.autopark.dto.TokenDto;

public interface LoginService {

    TokenDto login(UserForm userForm);

}
