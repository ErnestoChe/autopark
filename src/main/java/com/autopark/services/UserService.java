package com.autopark.services;

import com.autopark.dto.UserDto;

import java.util.List;

public interface UserService {

    UserDto findByUsername(String username);

    List<UserDto> findByRole(String role_name);

    UserDto giveRole(String username, String role);
}
