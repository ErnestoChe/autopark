package com.autopark.services;

import com.autopark.domain.enums.Roles;
import com.autopark.domain.form.UserForm;
import com.autopark.dto.UserDto;

public interface SignUpService {

    UserDto signUp(UserForm userForm, Roles role);

    default UserDto adminSignUp(UserForm userForm){
        return signUp(userForm, Roles.ROLE_ADMIN);
    }

}
