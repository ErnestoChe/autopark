package com.autopark.services.impl;

import com.autopark.dto.TokenDto;
import com.autopark.mapper.TokenMapper;
import com.autopark.repository.TokenRepository;
import com.autopark.services.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TokenServiceImpl implements TokenService {
    private final TokenRepository tokenRepository;
    private final TokenMapper tokenMapper;

    @Override
    public TokenDto findByUsername(String username) {
        return tokenMapper.toDto(tokenRepository.findByUser(username));
    }
}
