package com.autopark.services.impl;

import com.autopark.domain.Car;
import com.autopark.domain.CarType;
import com.autopark.domain.enums.TypeName;
import com.autopark.domain.form.CarForm;
import com.autopark.dto.CarDto;
import com.autopark.exceptions.NoSuchCarException;
import com.autopark.exceptions.NoSuchTypeException;
import com.autopark.mapper.CarMapper;
import com.autopark.repository.CarRepository;
import com.autopark.repository.CarTypeRepository;
import com.autopark.services.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    private final CarMapper carMapper;
    private final CarTypeRepository carTypeRepository;

    @Override
    public List<CarDto> findAllCars() {
        List<Car> all = carRepository.findAll();
        if (all.isEmpty()) {
            throw new NoSuchCarException("all car");
        }
        return carMapper.listToDto(all);
    }

    @Override
    public List<CarDto> findAllCarsPriceGreaterThan(int price) {
        List<Car> byPriceGreaterThan = carRepository.findByPriceGreaterThan(price);
        if (byPriceGreaterThan.isEmpty()) {
            throw new NoSuchCarException("by price");
        }
        return carMapper.listToDto(byPriceGreaterThan);
    }

    @Override
    public List<CarDto> findByColorEquals(String color) {
        List<Car> byColorEquals = carRepository.findByColorEquals(color);
        if (byColorEquals.isEmpty()) {
            throw new NoSuchCarException("by size");
        }
        return carMapper.listToDto(byColorEquals);
    }

    @Override
    public List<CarDto> findByType(String type) {
        if(Arrays.stream(TypeName.values()).noneMatch((t) -> t.name().equals(type.toUpperCase()))){
            throw new NoSuchTypeException(type);
        }
        CarType carType = carTypeRepository.findByType(TypeName.valueOf(type.toUpperCase()));
        List<Car> byType_id = carRepository.findByType_Id(carType.getId());
        if (byType_id.isEmpty()) {
            throw new NoSuchCarException("by type");
        }
        return carMapper.listToDto(byType_id);

    }

    @Override
    public List<CarDto> findByWeightBetween(int from, int to) {
        List<Car> byWeightBetween = carRepository.findByWeightBetween(from, to);
        if (byWeightBetween.isEmpty()) {
            throw new NoSuchCarException("by weight");
        }
        return carMapper.listToDto(byWeightBetween);
    }

    @Override
    public CarDto saveCar(CarForm carForm) {
        if(Arrays.stream(TypeName.values()).noneMatch((t) -> t.name().equals(carForm.getType()))){
            throw new NoSuchTypeException(carForm.getType());
        }
        CarType type = carTypeRepository.findByType(TypeName.valueOf(carForm.getType()));
        Car carToSave = Car.builder()
                .color(carForm.getColor())
                .price(carForm.getPrice())
                .weight(carForm.getWeight())
                .type(type)
                .build();
        Car r = carRepository.save(carToSave);
        return carMapper.toDto(r);
    }

    @Override
    public CarDto findById(long id) {
        Optional<Car> byId = carRepository.findById(id);
        if (byId.isEmpty()) {
            throw new NoSuchCarException("by id");
        }
        return carMapper.toDto(byId.get());
    }
}
