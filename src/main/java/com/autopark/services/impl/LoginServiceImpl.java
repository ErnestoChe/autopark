package com.autopark.services.impl;

import com.autopark.domain.Token;
import com.autopark.domain.User;
import com.autopark.domain.form.UserForm;
import com.autopark.dto.TokenDto;
import com.autopark.exceptions.UserNotFoundException;
import com.autopark.exceptions.WrongPasswordException;
import com.autopark.mapper.TokenMapper;
import com.autopark.repository.TokenRepository;
import com.autopark.repository.UserRepository;
import com.autopark.services.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class LoginServiceImpl implements LoginService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenRepository tokenRepository;
    private final TokenMapper tokenMapper;

    @Override
    public TokenDto login(UserForm userForm) {
        User userCandidate = userRepository.findByUsername(userForm.getUsername());
        if (userCandidate != null) {
            if (passwordEncoder.matches(userForm.getPassword(), userCandidate.getPassword())) {
                String value = userCandidate.getUsername()
                        + ":" + System.currentTimeMillis()
                        + ":" + userCandidate.getRoles().get(0).getName().name();
                Token token = Token.builder()
                        .user(userCandidate)
                        .value(value)
                        .build();
                Token save = tokenRepository.save(token);
                return tokenMapper.toDto(save);
            } else {
                throw new WrongPasswordException();
            }
        }
        throw new UserNotFoundException(userForm.getUsername());
    }
}
