package com.autopark.services.impl;

import com.autopark.domain.Role;
import com.autopark.domain.User;
import com.autopark.domain.enums.Roles;
import com.autopark.dto.UserDto;
import com.autopark.exceptions.NoSuchRoleException;
import com.autopark.exceptions.UserNotFoundException;
import com.autopark.mapper.RoleMapper;
import com.autopark.mapper.UserMapper;
import com.autopark.repository.RoleRepository;
import com.autopark.repository.UserRepository;
import com.autopark.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final RoleMapper roleMapper;
    private final RoleRepository roleRepository;

    @Override
    public UserDto findByUsername(String username) {
        User byUsername = userRepository.findByUsername(username);
        if (byUsername == null) {
            throw new UserNotFoundException(username);
        }
        UserDto userDto = userMapper.toDto(byUsername);
        userDto.setRoles(roleMapper.listToDto(userRepository.findByUsername(username).getRoles()));
        return userDto;
    }

    @Override
    public List<UserDto> findByRole(String role_name) {
        if (Arrays.stream(Roles.values()).noneMatch((t) -> t.name().equals(role_name))) {
            throw new NoSuchRoleException(role_name);
        }
        Role role = roleRepository.findByName(Roles.valueOf(role_name));
        return userMapper.listToDto(userRepository.findByRoles_Id(role.getId()));
    }

    @Override
    public UserDto giveRole(String username, String role) {
        if (Arrays.stream(Roles.values()).noneMatch((t) -> t.name().equals(role))) {
            throw new NoSuchRoleException(role);
        }
        User u = userRepository.findByUsername(username);
        if (u == null) {
            throw new UserNotFoundException(username);
        }
        List<Role> roles = u.getRoles();
        Role admin_role = roleRepository.findByName(Roles.valueOf(role));
        List<Role> roleList = new ArrayList<>(roles);
        if (!roleList.contains(admin_role)) {
            roleList.add(admin_role);
        }
        u.setRoles(roleList);
        return userMapper.toDto(userRepository.save(u));
    }
}
