package com.autopark.services.impl;

import com.autopark.domain.enums.TypeName;
import com.autopark.dto.CarTypeDto;
import com.autopark.mapper.CarTypeMapper;
import com.autopark.repository.CarTypeRepository;
import com.autopark.services.CarTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarTypeServiceImpl implements CarTypeService {

    private final CarTypeRepository carTypeRepository;
    private final CarTypeMapper carTypeMapper;

    @Override
    public CarTypeDto findById(long id) {
        return carTypeMapper.toDto(carTypeRepository.findById(id));
    }

    @Override
    public List<CarTypeDto> findAll() {
        return carTypeMapper.listToDto(carTypeRepository.findAll());
    }

    @Override
    public CarTypeDto findByType(String type_text) {
        return carTypeMapper.toDto(carTypeRepository.findByType(TypeName.valueOf(type_text)));
    }
}
