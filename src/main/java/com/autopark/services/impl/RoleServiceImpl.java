package com.autopark.services.impl;

import com.autopark.domain.enums.Roles;
import com.autopark.dto.RoleDto;
import com.autopark.mapper.RoleMapper;
import com.autopark.repository.RoleRepository;
import com.autopark.services.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;

    @Override
    public RoleDto findByName(String name) {
        return roleMapper.toDto(roleRepository.findByName(Roles.valueOf(name)));
    }
}
