package com.autopark.services.impl;

import com.autopark.domain.Role;
import com.autopark.domain.User;
import com.autopark.domain.enums.Roles;
import com.autopark.domain.form.UserForm;
import com.autopark.dto.UserDto;
import com.autopark.exceptions.UserAlreadyExistException;
import com.autopark.mapper.UserMapper;
import com.autopark.repository.RoleRepository;
import com.autopark.repository.UserRepository;
import com.autopark.services.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final RoleRepository roleRepository;

    @Override
    @Transactional
    public UserDto signUp(UserForm userForm, Roles roleName) {
        if (userRepository.findByUsername(userForm.getUsername()) == null) {
            Role role = roleRepository.findByName(roleName);
            User userToSave = User.builder()
                    .username(userForm.getUsername())
                    .password(userForm.getPassword())
                    .roles(Collections.singletonList(role))
                    .build();
            User saved = userRepository.save(userToSave);
            return userMapper.toDto(saved);
        } else {
            throw new UserAlreadyExistException(userForm.getUsername());
        }
    }
}
