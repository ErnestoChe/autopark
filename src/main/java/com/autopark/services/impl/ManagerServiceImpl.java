package com.autopark.services.impl;

import com.autopark.domain.Car;
import com.autopark.domain.CarImage;
import com.autopark.domain.CarType;
import com.autopark.domain.enums.TypeName;
import com.autopark.domain.form.CarForm;
import com.autopark.dto.CarDto;
import com.autopark.dto.CarImageDto;
import com.autopark.exceptions.NoSuchCarException;
import com.autopark.exceptions.NoSuchTypeException;
import com.autopark.mapper.CarImageMapper;
import com.autopark.mapper.CarMapper;
import com.autopark.repository.CarImageRepository;
import com.autopark.repository.CarRepository;
import com.autopark.services.ManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ManagerServiceImpl implements ManagerService {

    private final CarRepository carRepository;
    private final CarImageRepository carImageRepository;
    private final CarMapper carMapper;
    private final CarImageMapper carImageMapper;

    @Override
    /**
     * id
     * price
     * weight
     * color
     * type
     * updated car saves in repo with same id, but in the end of list when findAll() called
     */
    public CarDto updateCar(long id, CarForm carForm) {
        Optional<Car> byId = carRepository.findById(id);
        if(byId.isEmpty()){
            throw new NoSuchCarException(String.valueOf(id));
        }
        Car c = byId.get();
        String type = carForm.getType();
        if(type != null)
        if (Arrays.stream(TypeName.values()).noneMatch((t) -> t.name().equals(carForm.getType()))) {
            throw new NoSuchTypeException(carForm.getType());
        }
        CarType build = CarType.builder()
                .type(TypeName.valueOf(type == null ? c.getType().getType().name() : type))
                .build();
        c.setColor(carForm.getColor() == null ? c.getColor() : carForm.getColor());
        c.setPrice(carForm.getPrice() == null ? c.getPrice() : carForm.getPrice());
        c.setWeight(carForm.getWeight() == null ? c.getWeight() : carForm.getWeight());
        c.setType((build == null) ? c.getType() : build);
        return carMapper.toDto(carRepository.save(c));
    }

    @Override
    public void sellCar(long id) {
        Optional<Car> byId = carRepository.findById(id);
        if (byId.isEmpty()) {
            throw new NoSuchCarException(String.valueOf(id));
        }
        List<CarImage> byCar = carImageRepository.findByCar(byId.get());
        for (CarImage image : byCar) {
            carImageRepository.delete(image);
        }
        carRepository.deleteById(id);
    }

    @Override
    public CarDto addImage(long id, String url) {
        Optional<Car> byId = carRepository.findById(id);
        if(byId.isEmpty()){ throw new NoSuchCarException(String.valueOf(id)); }
        Car c = byId.get();
        List<CarImage> images = c.getImages();
        if (images == null) {
            images = new ArrayList<>();
        }
        images.add(CarImage.builder().imageUrl(url).car(c).build());
        c.setImages(images);
        Car save = carRepository.save(c);
        return carMapper.toDto(save);
    }

    @Override
    public List<CarImageDto> getImages(long id) {
        Optional<Car> byId = carRepository.findById(id);
        if (byId.isEmpty()) throw new NoSuchCarException(String.valueOf(id));
        return carImageMapper.listToDto(carImageRepository.findByCar(byId.get()));
    }
}
