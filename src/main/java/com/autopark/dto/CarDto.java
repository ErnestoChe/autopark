package com.autopark.dto;

import com.autopark.domain.CarImage;
import com.autopark.domain.CarType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "car fields")
public class CarDto {

    @ApiModelProperty(notes = "type of a certain car, example SEDAN, CROSSOVER")
    private CarTypeDto type;

    @ApiModelProperty(notes = "weight of a car")
    private Integer weight;

    @ApiModelProperty(notes = "color (not code, but Wd) example Red, Blue, Orange")
    private String color;

    @ApiModelProperty(notes = "price of car")
    private Integer price;

    //not in db, how to return??
    /*@ApiModelProperty(notes = "list of images for given car")
    private List<CarImage> imageList;*/
}
