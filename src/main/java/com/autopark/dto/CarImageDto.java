package com.autopark.dto;

import com.autopark.domain.Car;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CarImageDto {

    private String imageUrl;

    //makes an recursive loop in json files image->car->images->car... => stackoverflow
    //private Car car;

}
