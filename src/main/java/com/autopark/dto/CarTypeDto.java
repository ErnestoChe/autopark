package com.autopark.dto;

import com.autopark.domain.Car;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarTypeDto {

    private String type;

}
