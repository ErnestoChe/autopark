package com.autopark.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TokenDto {
    @ApiModelProperty(notes = "link to token's user")
    UserDto user;
    @ApiModelProperty(notes = "values of token, example user1:16...01:ROLE_USER")
    String value;
}

