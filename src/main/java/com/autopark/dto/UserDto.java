package com.autopark.dto;

import com.autopark.domain.Role;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {

    private String username;

    private List<RoleDto> roles;
}
