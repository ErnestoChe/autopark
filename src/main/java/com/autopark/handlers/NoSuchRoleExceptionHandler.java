package com.autopark.handlers;

import com.autopark.exceptions.ApiError;
import com.autopark.exceptions.NoSuchRoleException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class NoSuchRoleExceptionHandler {

    @ExceptionHandler(NoSuchRoleException.class)
    public ResponseEntity<ApiError> noSuchRole(NoSuchRoleException e){
        return new ResponseEntity<>(new ApiError(e.getMessage()), HttpStatus.CONFLICT);
    }

}
