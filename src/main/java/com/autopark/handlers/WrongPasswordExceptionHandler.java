package com.autopark.handlers;

import com.autopark.exceptions.ApiError;
import com.autopark.exceptions.WrongPasswordException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class WrongPasswordExceptionHandler {

    @ExceptionHandler(WrongPasswordException.class)
    public ResponseEntity<ApiError> wrongPass(WrongPasswordException e){
        return new ResponseEntity<>(new ApiError(e.getMessage()), HttpStatus.CONFLICT);
    }
}
