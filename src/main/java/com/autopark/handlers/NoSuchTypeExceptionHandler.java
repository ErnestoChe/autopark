package com.autopark.handlers;

import com.autopark.exceptions.ApiError;
import com.autopark.exceptions.NoSuchTypeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class NoSuchTypeExceptionHandler {

    @ExceptionHandler(NoSuchTypeException.class)
    public ResponseEntity<ApiError> noSuchType(NoSuchTypeException e){
        return new ResponseEntity<>(new ApiError(e.getMessage()), HttpStatus.CONFLICT);
    }
}
