package com.autopark.handlers;

import com.autopark.exceptions.ApiError;
import com.autopark.exceptions.NoSuchCarException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class NoSuchCarExceptionHandler {

    @ExceptionHandler(NoSuchCarException.class)
    public ResponseEntity<ApiError> noSuchCar(NoSuchCarException e){
        return new ResponseEntity<>(new ApiError(e.getMessage()), HttpStatus.CONFLICT);
    }

}
