package com.autopark.secuity.provider;

import com.autopark.domain.Token;
import com.autopark.repository.TokenRepository;
import com.autopark.secuity.authentication.TokenAuthentication;
import com.autopark.secuity.details.CustomUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TokenAuthProvider implements AuthenticationProvider {

    private final TokenRepository tokensRepository;

    private final CustomUserDetailsService userDetailsService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;

        Token tokenCandidate = tokensRepository.findByValue(tokenAuthentication.getName());

        if (tokenCandidate != null) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(tokenCandidate.getUser().getUsername());

            tokenAuthentication.setUserDetails(userDetails);
            tokenAuthentication.setAuthenticated(true);
            return tokenAuthentication;
        } else throw new IllegalArgumentException("Bad token");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return TokenAuthentication.class.equals(authentication);
    }
}
