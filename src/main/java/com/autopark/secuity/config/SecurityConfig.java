package com.autopark.secuity.config;

import com.autopark.secuity.filter.TokenFilter;
import com.autopark.secuity.provider.TokenAuthProvider;
import com.autopark.util.URIS;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;

import static com.autopark.util.URIS.*;

@Component
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final TokenAuthProvider authenticationProvider;
    private final TokenFilter tokenFilter;

    @Value("${selectbyusername}")
    private String usernameQuery;
    @Value("${selectusernameandrole}")
    private String authoritiesQuery;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery(usernameQuery)
                .authoritiesByUsernameQuery(authoritiesQuery);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .addFilterBefore(tokenFilter, BasicAuthenticationFilter.class)
                .antMatcher("/**")
                .authenticationProvider(authenticationProvider)
                .authorizeRequests()
                .antMatchers(CAR_URI+ALL_CARS_URI, CAR_URI+BY+"/**").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                .antMatchers(USER_URI+ADD_ROLE).hasAuthority("ROLE_ADMIN")
                .antMatchers(MANAGER_URI, CAR_URI+ADD_CAR).hasAnyAuthority("ROLE_MANAGER", "ROLE_ADMIN")
                .antMatchers(URIS.AUTH).permitAll();
        http.csrf().disable();
    }

    @Override
    public void configure(WebSecurity webSecurity) {
        webSecurity.ignoring().antMatchers(AUTH);
    }

    @Bean
    public static NoOpPasswordEncoder passwordEncoder() {
        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    }
}
