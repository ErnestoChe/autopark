package com.autopark.secuity.filter;

import com.autopark.secuity.authentication.TokenAuthentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class TokenFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        TokenAuthentication tokenAuthentication;
        try{
            String tokenH = request.getHeader("Authorization").substring(7);
            tokenAuthentication = new TokenAuthentication(tokenH);
            SecurityContextHolder.getContext().setAuthentication(tokenAuthentication);
        }catch (NullPointerException e){
            tokenAuthentication = new TokenAuthentication(null);
            tokenAuthentication.setAuthenticated(false);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

}
