package com.autopark.domain.form;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CarForm {

    private String type;

    private String color;

    private Integer weight;

    private Integer price;

}
