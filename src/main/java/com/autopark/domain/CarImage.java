package com.autopark.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CarImage extends BaseEntity{

    private String imageUrl;

    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;
}
