package com.autopark.domain.enums;

public enum TypeName {
    CROSSOVER, SEDAN, HATCHBACK, SPORTCAR;
}
