package com.autopark.domain;

import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Car extends BaseEntity {

    @ManyToOne(cascade = CascadeType.ALL/*, fetch = FetchType.EAGER*/)
    private CarType type;

    private Integer weight;

    private String color;

    private Integer price;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    private List<CarImage> images;

}
