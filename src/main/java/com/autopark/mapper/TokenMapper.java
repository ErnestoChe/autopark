package com.autopark.mapper;

import com.autopark.domain.Token;
import com.autopark.dto.TokenDto;
import org.mapstruct.Mapper;

@Mapper
public interface TokenMapper {

    TokenDto toDto(Token token);

}

