package com.autopark.mapper;

import com.autopark.domain.CarImage;
import com.autopark.dto.CarImageDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CarImageMapper {

    CarImageDto toDto(CarImage image);

    List<CarImageDto> listToDto(List<CarImage> images);

}
