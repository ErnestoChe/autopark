package com.autopark.mapper;

import com.autopark.domain.Role;
import com.autopark.dto.RoleDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper {

    RoleDto toDto(Role role);

    List<RoleDto> listToDto(List<Role> roles);
}

