package com.autopark.mapper;

import com.autopark.domain.User;
import com.autopark.dto.UserDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    UserDto toDto(User user);

    List<UserDto> listToDto(List<User> users);
}
