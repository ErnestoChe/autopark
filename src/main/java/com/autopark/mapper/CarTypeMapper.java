package com.autopark.mapper;

import com.autopark.domain.CarType;
import com.autopark.dto.CarTypeDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CarTypeMapper {

    CarTypeDto toDto(CarType carType);

    List<CarTypeDto> listToDto(List<CarType> types);
}
