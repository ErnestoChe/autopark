package com.autopark.controllers;

import com.autopark.domain.form.CarForm;
import com.autopark.dto.CarDto;
import com.autopark.dto.CarImageDto;
import com.autopark.services.ManagerService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.autopark.util.URIS.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(MANAGER_URI)
public class ManagerController {

    private final ManagerService managerService;

    @RequestMapping(value = UPDATE_CAR, method = RequestMethod.POST)
    @ApiOperation(
            value = "upd car with carForm"
    )
    public CarDto updateCar(
            @RequestBody CarForm carForm,
            @RequestParam("id") int id) {
        return managerService.updateCar(id, carForm);
    }

    @RequestMapping(value = SELL_CAR, method = RequestMethod.POST)
    @ApiOperation(
            value = "sell car by id "
    )
    public ResponseEntity<?> sellCar(@RequestParam("id") long id) {
        managerService.sellCar(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @RequestMapping(value = ADD_IMAGE, method = RequestMethod.POST)
    @ApiOperation(
            value = "add img to a car by id",
            notes = " img is just an url string "
    )
    public CarDto addImage(
            @RequestParam("id") long id,
            @RequestParam("url") String url) {
        return managerService.addImage(id, url);
    }

    @RequestMapping(value = "/images", method = RequestMethod.POST)
    public List<CarImageDto> images(@RequestParam("id") long id) {
        return managerService.getImages(id);
    }
}
