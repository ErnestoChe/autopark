package com.autopark.controllers;

import com.autopark.domain.form.UserForm;
import com.autopark.dto.TokenDto;
import com.autopark.services.LoginService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.autopark.util.URIS.AUTH;

@RestController
@RequiredArgsConstructor
@RequestMapping
public class LoginController {

    private final LoginService loginService;

    @RequestMapping(value = AUTH, method = RequestMethod.POST)
    @ApiOperation(
            value = "create token with user credentials ",
            notes = "send in userForm",
            response = TokenDto.class
    )
    public ResponseEntity<?> createAuthenticationToken(@RequestBody UserForm userForm) throws Exception {
        TokenDto tokenDto = loginService.login(userForm);
        return ResponseEntity.ok(tokenDto.getValue());
    }


}
