package com.autopark.controllers;

import com.autopark.domain.enums.Roles;
import com.autopark.domain.form.UserForm;
import com.autopark.dto.UserDto;
import com.autopark.services.SignUpService;
import com.autopark.services.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.autopark.util.URIS.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(USER_URI)
public class UserController {

    private final SignUpService signUpService;
    private final UserService userService;

    @RequestMapping(value = USER_SIGNUP, method = RequestMethod.POST)
    @ApiOperation(
            value = "sign up with new username/password ",
            notes = "User reg",
            response = HttpStatus.class
    )
    public UserDto signUp(@RequestBody UserForm userForm) {
        return signUpService.signUp(userForm, Roles.ROLE_USER);
    }

    @RequestMapping(value = ADMIN_SIGNUP, method = RequestMethod.POST)
    @ApiOperation(
            value = "sign up with new username/password ",
            notes = "Admin reg",
            response = HttpStatus.class
    )
    public UserDto adminSignUp(@RequestBody UserForm userForm) {
        return signUpService.adminSignUp(userForm);
    }

    /**
     * @param username example "user"
     * @param role     example "ROLE_ADMIN", "ROLE_MANAGER"...
     */
    @RequestMapping(value = ADD_ROLE, method = RequestMethod.POST)
    @ApiOperation(
            value = "add role to an existing user",
            response = HttpStatus.class
    )
    public UserDto addRole(
            @RequestParam("username") String username,
            @RequestParam("role") String role) {
        return userService.giveRole(username, role);
    }
}
