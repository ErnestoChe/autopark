package com.autopark.controllers;

import com.autopark.domain.form.CarForm;
import com.autopark.dto.CarDto;
import com.autopark.services.CarService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.autopark.util.URIS.ADD_CAR;
import static com.autopark.util.URIS.CAR_URI;

@RestController
@RequiredArgsConstructor
@RequestMapping(CAR_URI)
public class CarAddController {

    private final CarService carService;

    @RequestMapping(value = ADD_CAR, method = RequestMethod.POST)
    @ApiOperation(
            value = "adds car via carform",
            notes = "type in upper case, color and weight are int values, color string",
            response = HttpStatus.class
    )
    public CarDto addCar(@RequestBody CarForm carForm) {
        return carService.saveCar(carForm);
    }
}
