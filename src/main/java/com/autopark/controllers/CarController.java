package com.autopark.controllers;

import com.autopark.dto.CarDto;
import com.autopark.services.CarService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.autopark.util.URIS.*;


@RestController
@RequiredArgsConstructor
@RequestMapping(CAR_URI)
public class CarController {

    private final CarService carService;

    // http/https//:ip:port/car/all

    @GetMapping(ALL_CARS_URI)
    @ApiOperation(
            value = "get all cars from DB",
            notes = "nothing here",
            response = CarDto.class
    )
    public List<CarDto> getAll() {
        System.out.println("all cars debug");
        return carService.findAllCars();
    }

    @GetMapping(BY + PRICE_CAR_URI)
    @ApiOperation(
            value = "get cars from DB by price ",
            notes = "example price = 100, gets all cars with price greater than given price",
            response = CarDto.class
    )
    public List<CarDto> getGreaterThanPrice(@RequestParam("price") int price) {
        return carService.findAllCarsPriceGreaterThan(price);
    }

    @GetMapping(BY + COLOR_CAR_URI)
    @ApiOperation(
            value = "get cars from DB by color",
            notes = "color by word, not code, example Red, Blue",
            response = CarDto.class
    )
    public List<CarDto> getByColor(@RequestParam("color") String color) {
        return carService.findByColorEquals(color);
    }

    @GetMapping(BY + TYPE_CAR_URI)
    @ApiOperation(
            value = "get cars of a given type from DB",
            notes = "types in upper case",
            response = CarDto.class
    )
    public List<CarDto> getByType(@RequestParam("type") String type) {
        return carService.findByType(type);
    }

    @GetMapping(BY + WEIGHT_CAR_URI)
    @ApiOperation(
            value = "get cars from DB with weight between 2 values",
            notes = "give to params",
            response = CarDto.class
    )
    public List<CarDto> getByType(@RequestParam("from") int from, @RequestParam("to") int to) {
        return carService.findByWeightBetween(from, to);
    }

    @GetMapping(BY + ID_CAR_URI)
    @ApiOperation(
            value = "get car by id",
            response = CarDto.class
    )
    public CarDto getById(@RequestParam("id") long id) {
        return carService.findById(id);
    }
}
