package com.autopark.repository;

import com.autopark.domain.CarType;
import com.autopark.domain.enums.TypeName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarTypeRepository extends JpaRepository<CarType, Long> {

    CarType findById(long id);

    List<CarType> findAll();

    CarType findByType(TypeName textName);

}
