package com.autopark.repository;

import com.autopark.domain.Role;
import com.autopark.domain.enums.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(Roles role);

}