package com.autopark.repository;

import com.autopark.domain.Car;
import com.autopark.domain.form.CarForm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    List<Car> findByPriceGreaterThan(int price);

    List<Car> findByColorEquals(String color);

    List<Car> findByType_Id(long id);

    List<Car> findByWeightBetween(int from, int to);

    @Transactional
    <S extends Car> S save(CarForm carForm);
}
