package com.autopark.repository;

import com.autopark.domain.Car;
import com.autopark.domain.CarImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarImageRepository extends JpaRepository<CarImage, Long> {

    List<CarImage> findByCar(Car car);

    CarImage findByImageUrl(String url);

}
