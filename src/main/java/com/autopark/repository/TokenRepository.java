package com.autopark.repository;

import com.autopark.domain.Token;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenRepository extends JpaRepository<Token, Long> {

    Token findByUser(String username);

    Token findByValue(String val);

}

