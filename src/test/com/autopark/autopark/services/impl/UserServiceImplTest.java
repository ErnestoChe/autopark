package com.autopark.autopark.services.impl;

import com.autopark.autopark.AbstractTest;
import com.autopark.domain.Role;
import com.autopark.domain.enums.Roles;
import com.autopark.domain.form.UserForm;
import com.autopark.dto.UserDto;
import com.autopark.services.SignUpService;
import com.autopark.services.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserServiceImplTest extends AbstractTest {
    @Autowired
    UserServiceImpl userService;
    @Autowired
    SignUpService signUpService;
    @BeforeEach
    void init(){
        List<Role> roles = roleRepository.findAll();
        if (roles.isEmpty()) {
            roles.add(Role.builder()
                    .name(Roles.ROLE_USER)
                    .build());
            roles.add(Role.builder()
                    .name(Roles.ROLE_ADMIN)
                    .build());
            roleRepository.saveAll(roles);
        }
        UserForm userForm = UserForm.builder().username("user").password("pass").build();
        signUpService.signUp(userForm, Roles.ROLE_USER);
    }

    @Test
    void findUserByUsername(){
        UserDto userDto = userService.findByUsername("user");
        System.out.println();
        assertEquals(userDto.getRoles().get(0).getName(), Roles.ROLE_USER.name());
    }
    @Test
    void findUsersByRole(){
        List<UserDto> list = userService.findByRole(Roles.ROLE_ADMIN.name());
        assertEquals(list.size(), 0);
    }
}

