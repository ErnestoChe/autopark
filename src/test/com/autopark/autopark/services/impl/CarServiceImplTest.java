package com.autopark.autopark.services.impl;

import com.autopark.autopark.AbstractTest;
import com.autopark.domain.Car;
import com.autopark.domain.CarType;
import com.autopark.domain.enums.TypeName;
import com.autopark.domain.form.CarForm;
import com.autopark.dto.CarDto;
import com.autopark.mapper.CarMapper;
import com.autopark.services.impl.CarServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


class CarServiceImplTest extends AbstractTest {

    @Autowired
    CarServiceImpl carService;
    @Autowired
    CarMapper carMapper;
    @BeforeEach
    void init(){
        List<CarType> carTypes = carTypeRepository.findAll();
        if (carTypes.isEmpty()) {
            for (TypeName t: TypeName.values()) {
                carTypes.add(CarType.builder().type(t).build());
            }
            carTypeRepository.saveAll(carTypes);
        }
        CarType sport = carTypeRepository.findByType(TypeName.SPORTCAR);
        CarType cross = carTypeRepository.findByType(TypeName.CROSSOVER);
        CarType hatch = carTypeRepository.findByType(TypeName.HATCHBACK);
        CarType sedan = carTypeRepository.findByType(TypeName.SEDAN);

        Car[] cars = {
                Car
                        .builder()
                        .color("red")
                        .price(1001)
                        .weight(500)
                        .type(sport)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(1002)
                        .weight(1000)
                        .type(cross)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(1003)
                        .weight(1000)
                        .type(hatch)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(400)
                        .weight(1000)
                        .type(sedan)
                        .build()
        };

        when(carRepository.findAll()).thenReturn(Arrays.asList(cars));
        when(carRepository.findByPriceGreaterThan(500)).thenReturn(Arrays.stream(cars).filter(car -> car.getPrice() > 500).collect(Collectors.toList()));
    }

    @Test
    void findAllCars() {
        List<CarDto> allCars = carService.findAllCars();
        int size = allCars.size();

        assertEquals(size,4);
    }
    @Test
    void findAllCarsPrice(){
        List<CarDto> priceCars = carService.findAllCarsPriceGreaterThan(500);
        assertEquals(priceCars.size(), 3);
    }
}
