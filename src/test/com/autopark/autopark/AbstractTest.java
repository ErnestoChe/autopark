package com.autopark.autopark;

import com.autopark.repository.CarRepository;
import com.autopark.repository.CarTypeRepository;
import com.autopark.repository.RoleRepository;
import com.autopark.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AbstractTest {

    @LocalServerPort
    protected int port;

    protected TestRestTemplate testRestTemplate;

    @MockBean
    protected CarRepository carRepository;
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected CarTypeRepository carTypeRepository;
    @Autowired
    protected RoleRepository roleRepository;

    @AfterEach
    void flush(){
//        carRepository.deleteAll();
//        carRepository.flush();
        userRepository.deleteAll();
        userRepository.flush();
        roleRepository.deleteAll();
        roleRepository.flush();
    }
}
