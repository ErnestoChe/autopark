package com.autopark.autopark.controllers;

import com.autopark.autopark.AbstractTest;
import com.autopark.domain.User;
import com.autopark.dto.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;

import java.util.Collections;

import static com.autopark.util.URIS.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest extends AbstractTest {
    @BeforeEach
    void init(){
        userRepository.save(
                User
                        .builder()
                        .username("usr")
                        .password("pass")
                        //.role(Role.builder().name(ERole.ROLE_USER).build())
                        .build()
        );
    }

    @Test
    void getByUsername() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        testRestTemplate = new TestRestTemplate();
        ResponseEntity<UserDto> responseEntity = testRestTemplate
                .exchange("http://localhost:" + port + USER_URI + USER_NAME + "?username=usr",
                        HttpMethod.GET,
                        entity,
                        new ParameterizedTypeReference<>() {
                        });

        HttpStatus statusCode = responseEntity.getStatusCode();

        UserDto user = responseEntity.getBody();

        assertEquals(statusCode, HttpStatus.OK);
        //assertEquals(user.getPassword(), "pass");
    }
    @Test
    void signUpUser() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        testRestTemplate = new TestRestTemplate();
        ResponseEntity<UserDto> responseEntity = testRestTemplate
                .exchange("http://localhost:" + port + USER_URI + USER_SIGNUP +
                                "?username=user1&password=pass1",
                        HttpMethod.POST,
                        entity,
                        new ParameterizedTypeReference<>() {
                        });

        HttpStatus statusCode = responseEntity.getStatusCode();
        assertEquals(statusCode, HttpStatus.OK);
    }
}
