package test.com.autopark.autopark.controllers;

import com.autopark.autopark.AbstractTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/*
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CarControllerTest extends AbstractTest {

    @BeforeEach
    void init(){
        List<CarType> carTypes = carTypeRepository.findAll();
        if (carTypes.isEmpty()) {
            carTypes.add(CarType.builder()
                    .type(TypeName.CROSSOVER)
                    .build());
            carTypes.add(CarType.builder()
                    .type(ETypeName.HATCHBACK.name())
                    .type_name(ETypeName.HATCHBACK.name())
                    .build());
            carTypes.add(CarType.builder()
                    .type(ETypeName.SEDAN.name())
                    .type_name(ETypeName.SEDAN.name())
                    .build());
            carTypes.add(CarType.builder()
                    .type(ETypeName.SPORTCAR.name())
                    .type_name(ETypeName.SPORTCAR.name())
                    .build());
            carTypeRepository.saveAll(carTypes);
        }
        CarType sport = carTypeRepository.findByType("SPORTCAR");
        CarType cross = carTypeRepository.findByType("CROSSOVER");
        CarType hatch = carTypeRepository.findByType("HATCHBACK");
        CarType sedan = carTypeRepository.findByType("SEDAN");

        Car[] cars = {
                Car
                        .builder()
                        .color("red")
                        .price(1001)
                        .weight(500)
                        .type(sport)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(1002)
                        .weight(1000)
                        .type(cross)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(1003)
                        .weight(1000)
                        .type(hatch)
                        .build(),
                Car
                        .builder()
                        .color("blue")
                        .price(400)
                        .weight(1000)
                        .type(sedan)
                        .build()
        };
        carRepository.saveAll(Arrays.asList(cars.clone()));
    }


    @Test
    void getAll() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        testRestTemplate = new TestRestTemplate();
        ResponseEntity<List<CarDto>> responseEntity = testRestTemplate
                .exchange("http://localhost:" + port + CAR_URI + ALL_CARS_URI,
                        HttpMethod.GET,
                        entity,
                        new ParameterizedTypeReference<>() {
                        });

        HttpStatus statusCode = responseEntity.getStatusCode();

        List<CarDto> body = responseEntity.getBody();
        int size = body.size();

        assertEquals(statusCode, HttpStatus.OK);
        assertEquals(size,4);
    }

    @Test
    void getGreaterThanPrice(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        testRestTemplate = new TestRestTemplate();
        ResponseEntity<List<CarDto>> responseEntity = testRestTemplate
                .exchange("http://localhost:" + port + CAR_URI + PRICE_CAR_URI+ "?price=300",
                        HttpMethod.GET,
                        entity,
                        new ParameterizedTypeReference<>() {
                        });

        HttpStatus statusCode = responseEntity.getStatusCode();

        List<CarDto> body = responseEntity.getBody();
        String color = body.get(0).getColor();
        int size = body.size();

        assertEquals(statusCode, HttpStatus.OK);

        assertEquals(size,4);
        assertEquals(color, "red");
    }

    @Test
    void getByColor(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        testRestTemplate = new TestRestTemplate();
        ResponseEntity<List<CarDto>> responseEntity = testRestTemplate
                .exchange("http://localhost:" + port + CAR_URI + COLOR_CAR_URI+ "?color=red",
                        HttpMethod.GET,
                        entity,
                        new ParameterizedTypeReference<>() {
                        });

        HttpStatus statusCode = responseEntity.getStatusCode();

        List<CarDto> body = responseEntity.getBody();
        int size = body.size();

        assertEquals(statusCode, HttpStatus.OK);

        assertEquals(size,1);
    }
    @Test
    void getByType(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        testRestTemplate = new TestRestTemplate();
        ResponseEntity<List<CarDto>> responseEntity = testRestTemplate
                .exchange("http://localhost:" + port + CAR_URI + TYPE_CAR_URI+ "?type=sedan",
                        HttpMethod.GET,
                        entity,
                        new ParameterizedTypeReference<>() {
                        });

        HttpStatus statusCode = responseEntity.getStatusCode();

        List<CarDto> body = responseEntity.getBody();
        int size = body.size();

        assertEquals(statusCode, HttpStatus.OK);
        assertEquals(size,1);
    }

    @Test
    void getByWeight(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        testRestTemplate = new TestRestTemplate();
        ResponseEntity<List<CarDto>> responseEntity = testRestTemplate
                .exchange("http://localhost:" + port + CAR_URI + WEIGHT_CAR_URI+ "?from=750&to=1500",
                        HttpMethod.GET,
                        entity,
                        new ParameterizedTypeReference<>() {
                        });

        HttpStatus statusCode = responseEntity.getStatusCode();

        List<CarDto> body = responseEntity.getBody();
        int size = body.size();

        assertEquals(statusCode, HttpStatus.OK);

        assertEquals(size,3);
    }

    @Test
    void addCar(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        testRestTemplate = new TestRestTemplate();
        ResponseEntity<List<CarDto>> responseEntity = testRestTemplate
                .exchange("http://localhost:" + port + CAR_URI + ADD_CAR +
                                "?color=red&price=100&weight=100&type=SEDAN",
                        HttpMethod.POST,
                        entity,
                        new ParameterizedTypeReference<>() {
                        });

        HttpStatus statusCode = responseEntity.getStatusCode();
        List<Car> byWeightBetween = carRepository.findByWeightBetween(99, 101);
        assertEquals(byWeightBetween.size(), 1);
        CarType carType = carTypeRepository.findByType(byWeightBetween.get(0).getType().getType_name());

        System.out.println();
        assertEquals(statusCode, HttpStatus.CREATED);
    }
}*/
